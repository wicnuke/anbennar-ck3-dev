﻿###################################################
# ONGOING EVENTS FOR ENHANCE_ABILITY
###################################################
namespace = anb_enhance_ability_ongoing

#Target is good at skill category 
anb_enhance_ability_ongoing.1 = {
	type = character_event
	title = anb_enhance_ability_ongoing.1.t
	desc = anb_enhance_ability_ongoing.1.d

	theme = learning

	trigger = {
		NOT = { scope:scheme = { has_scheme_modifier = enhance_ability_working_harder } }
		scope:target = {
			OR = {
				AND = {
					has_character_flag = enhance_ability_diplomacy
					scope:target.diplomacy > excellent_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_martial
					scope:target.martial > excellent_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_stewardship
					scope:target.stewardship > excellent_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_intrigue
					scope:target.intrigue > excellent_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_learning
					scope:target.learning > excellent_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_prowess
					scope:target.prowess > excellent_skill_level
				}
			}
		}
	}

	#Try harder
	option = {
		name = anb_enhance_ability_ongoing.1.a
		stress_impact = {
			base = medium_stress_impact_gain
			magical_affinity_1 = minor_stress_impact_loss
			magical_affinity_2 = minor_stress_impact_loss
			magical_affinity_3 = medium_stress_impact_loss
			diligent = minor_stress_impact_loss
			lazy = minor_stress_impact_gain
		}
		scope:scheme = {
			add_scheme_modifier = {
				type = enhance_ability_working_harder
				years = 2
			}
			add_scheme_progress = 2
		}
	}

	#Same effort
	option = {
		name = anb_enhance_ability_ongoing.1.b
		stress_impact = {
			diligent = minor_stress_impact_gain
		}
	}
}

#Target is bad at skill category
anb_enhance_ability_ongoing.2 = {
	type = character_event
	title = anb_enhance_ability_ongoing.2.t
	desc = anb_enhance_ability_ongoing.2.d

	theme = learning

	trigger = {
		NOT = { scope:scheme = { has_scheme_modifier = enhance_ability_working_harder } } 
		scope:target = {
			OR = {
				AND = {
					has_character_flag = enhance_ability_diplomacy
					scope:target.diplomacy < average_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_martial
					scope:target.martial < average_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_stewardship
					scope:target.stewardship < average_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_intrigue
					scope:target.intrigue < average_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_learning
					scope:target.learning < average_skill_level
				}
				AND = {
					has_character_flag = enhance_ability_prowess
					scope:target.prowess < average_skill_level
				}
			}
		}
	}
	
	#Same effort
	option = {
		name = anb_enhance_ability_ongoing.2.a
		scope:scheme = {
			add_scheme_modifier = {
				type = enhance_ability_working_harder
				years = 2
			}
		}
	}

	#Let off a little
	option = {
		name = anb_enhance_ability_ongoing.2.b
		stress_impact = {
			base = minor_stress_impact_loss
			diligent = medium_stress_impact_gain
		}
		scope:scheme = {
			add_scheme_progress = -2
		}
	}
}

#Target pushes for you to try harder
anb_enhance_ability_ongoing.3 = {
	type = character_event
	title = anb_enhance_ability_ongoing.3.t
	desc = anb_enhance_ability_ongoing.3.d

	theme = friendly
	
	left_portrait = {
		character = scope:owner
		animation = idle
	}
	right_portrait = {
		trigger = { NOT = { scope:owner = scope:target } }
		character = scope:target
		animation = beg
	}

	trigger = {
		NOT = { scope:scheme = { has_scheme_modifier = enhance_ability_working_target_pushes_working_harder } }
		NOT = { scope:owner = scope:target }
	}

	#Acquiesce
	option = {
		name = anb_enhance_ability_ongoing.3.a
		ai_chance = {
			base = 50
		}
		reverse_add_opinion = {
			target = scope:target
			modifier = grateful_opinion
			opinion = 20
		}
		stress_impact = {
			base = medium_stress_impact_gain
			magical_affinity_1 = minor_stress_impact_loss
			magical_affinity_2 = minor_stress_impact_loss
			magical_affinity_3 = medium_stress_impact_loss
		}
		scope:scheme = {
			add_scheme_progress = 2
			add_scheme_modifier = {
				type = enhance_ability_working_target_pushes_working_harder
				years = 2
			}
		}
		
	}

	#Who is the mage here again?
	option = {
		name = anb_enhance_ability_ongoing.3.b
		ai_chance = {
			base = 50
			modifier = {
				factor = 2
				scope:owner = {
					has_trait = arrogant
				}
			}
		}
		add_opinion = {
			target = scope:target
			modifier = dismissive_opinion
			opinion = -20
		}
	}

}

#Find a fitting material component
#Idea is that different component will increase effectiveness differently for different skills
anb_enhance_ability_ongoing.4 = {
	type = character_event
	title = anb_enhance_ability_ongoing.4.t
	desc = anb_enhance_ability_ongoing.4.d

	theme = medicine

	trigger = {
		scope:scheme = { NOT = { has_variable = had_found_material_component_event } }
	}

	#More likely to happen when material component hasn't been selected yet
	weight_multiplier = {
		base = 0.5
		modifier = {
			add = 1
			scope:scheme = { NOT = { has_variable = had_found_material_component_event } }
		}
	}

			
	immediate = {
		scope:scheme = {
			set_variable  = {
				name = had_found_material_component_event
				value = yes
			}
		}
	}

	#Some component
	option = {
		name = anb_enhance_ability_ongoing.4.a
		#TODO
	}

	#Some component
	option = {
		name = anb_enhance_ability_ongoing.4.b
		#TODO
	}

	#Some component
	option = {
		name = anb_enhance_ability_ongoing.4.c
		#TODO
	}

	#Some component
	option = {
		name = anb_enhance_ability_ongoing.4.e
		#TODO
	}

	#I ain't need no stinkin' component
	option = {
		name = anb_enhance_ability_ongoing.4.f
		#TODO
	}
}